
const app = require('express')()

const fetch = require('node-fetch')

const http = require('http').createServer(app)

const { OAuth2Client } = require('google-auth-library')

const CLIENT_ID = '991271249488-9sfaf70fsa4oanjmr41ed0j9b5kgdphm.apps.googleusercontent.com'
const oAuthClient = new OAuth2Client(CLIENT_ID)

app.use(require('cors')())
app.use(require('body-parser').json())

/** 
 * 
 * So that we can use it on our localhost with headers.
 */
const io = require('socket.io')(http, {
  handlePreflightRequest: (req, res) => {
    const headers = {
      'Access-Control-Allow-Headers': 'Content-Type, Authorization',
      'Access-Control-Allow-Origin': req.headers.origin, // or the specific origin you want to give access to,
      'Access-Control-Allow-Credentials': true
    }
    res.writeHead(200, headers)
    res.end()
  }
})

/**
 * Our rooms on the server
 */
const rooms = []
//Everyone online
const online = {}
//Default servers.
rooms.push({
  name: 'General',
  key: false,
  default: true,
  messages: []
})
rooms.push({
  name: 'Public',
  key: false,
  default: true,
  messages: []
})
/**
 * Authentication middleware
 */
io.use((socket, next) => {
  const token = socket.request.headers.authorization
  try {
    if (token === undefined) {
      throw error('undefined')
    }
    verify(token)
      .then(resp => {
        socket.request.email = resp.email
        socket.request.pic = resp.picture
        online[socket.id] = { username: resp.email, pic: resp.picture }
        return next()
      })
  } catch (err) {
    socket.emit('resetConnection') //Logs the user out and reload the site.
    console.log(err.message)
  }
})
io.on('connection', (client) => {
  /**
 * On first connnect.
 */


  /**
   * On our first connect we just wanna enter the rooms which has the default tag.
   */
  client.on('init', () => {
    const defaultRooms = []
    rooms.forEach(room => {
      if (room.default) {
        const cRoom = { ...room }
        cRoom.online = registerToRoom(room.name, client)
        defaultRooms.push(cRoom)
      }
    })
    client.emit('init', defaultRooms)
  })




  /** 
   * When we join a room we need to check so that the room exists
   * so that we have the right key.
   * We send error messages if it doesn't follow the requierments. 
   */
  client.on('joinRoom', (data) => {
    if (data.name) {
      for (let i = 0; i < rooms.length; i++) {
        const room = rooms[i]
        if (room.name === data.name) {
          if (room.key) {
            if (data.key === undefined || data.key !== room.key) {
              client.emit('nonFatalError', 'Invalid password to room ' + room.name)
              return false
            }
          }
          const cRoom = { ...room }

          cRoom.online = registerToRoom(data.name, client)
          client.emit('joinedRoom', cRoom)
          client.emit('alertMessage', 'Sucessfully joined: ' + data.name)
          return true
        }
      }
      client.emit('nonFatalError', 'The room ' + data.name + ' could not be found.')
      return false
    }
    client.emit('nonFatalError', 'You cant join a room without a name.')
  })

  /**
   * When sending a message we controll if it's a giphy and if it's a private message.
   */
  client.on('message', async ({ message, room, color }) => {
    if (client.rooms[room]) {
      let giphy = false
      //If a giphy we remove the [giphy] from the actual message.
      if (message.includes('[') && message.includes(']')) {
        const giphyTerm = message.split('[').pop().split(']')[0] // Gets last giphy

        message = message.substring(0, message.lastIndexOf('[')) // Removes giphy.
        try {
          giphy = await getGiphyUrl(giphyTerm)
          console.log(giphy)
        } catch (err) {
          client.emit('nonFatalError', 'Failed to giphy..')
          console.log(err)
        }
      }
      //If we have /message in the code we try to send the message to only the referenced person.
      if (message.includes('/message')) {
        const split = message.split('@')
        if (split.length > 2) {
          const username = split[1]
          const message = split[2]
          for (const socket in io.sockets.adapter.rooms[room].sockets) {
            if (online[socket] && online[socket].username.split('@')[0] === username) {
              io.to(socket).emit('message', { message, room: room, username: client.request.email, pic: client.request.pic, date: Date.now(), privateMsg: true, color, giphy })
              client.emit('message', { message, room: room, username: client.request.email, pic: client.request.pic, date: Date.now(), privateMsg: true, color, giphy })
              break
            }
          }
        }
      } else {
        //Normal message, send it to the room.
        io.to(room).emit('message', { message, room: room, username: client.request.email, pic: client.request.pic, date: Date.now(), privateMsg: false, color, giphy })
      }
    }
  })
  /**
   * CreateRoom
   * Requierments
   *  Need to have a name,
   *  the name can't be used before.
   *  The creator auto joins the room.
   */
  client.on('createRoom', ({ name, key }) => {
    let found = false
    if (name.trim() === '') {
      return client.emit('nonFatalError', 'Failed to create : Name cant be empty')
    }
    rooms.forEach(room => {
      if (room.name === name) {
        found = true
      }
    })
    if (found) {
      client.emit('nonFatalError', 'Failed to create : The room already exists.')
    } else {
      const online = registerToRoom(name, client)
      const length = rooms.push({
        name: name,
        key: key,
        default: false,
        messages: [],
        online
      })
      client.emit('joinedRoom', rooms[length - 1])
      client.emit('alertMessage', 'Succesfully created: ' + name)
    }
  })
  //If someone is disconnecting we want to remove that person from the online list.
  client.on('disconnecting', () => {
    for (const room in client.rooms) {
      getOnlineList(room, true, client.id)
    }
  })
})

const registerToRoom = (name, client) => {
  client.join(name)
  io.to(name).emit('message', { room: name, username: 'SERVER', message: client.request.email + ' just joined the room.' })
  return getOnlineList(name, true)
}

const getOnlineList = (name, emit, ignoreSocket = null) => {
  const currentlyOnline = []
  for (const socket in io.sockets.adapter.rooms[name].sockets) {
    if (socket !== ignoreSocket && online[socket]) {
      currentlyOnline.push(online[socket])
    }
  }
  emit && io.to(name).emit('currentlyOnline', { online: currentlyOnline, name })
  return currentlyOnline
}

app.post('/verify', (req, res) => {
  verify(req.body.token)
    .then((resp) => res.sendStatus(200))
    .catch((err) => {
      console.log(err)
      res.sendStatus(403)
    })
})
//Verifytoken against google
async function verify (token) {
  return new Promise((resolve, reject) => {
    oAuthClient.verifyIdToken({
      idToken: token,
      audience: CLIENT_ID
    })
      .then(resp => resolve(resp.payload))
      .catch(er => reject(er))
  })
}

async function getGiphyUrl (searchTerm) {
  return new Promise((resolve, reject) => {
    fetch('http://api.giphy.com/v1/gifs/search?api_key=yNZWH5hSknZltzBsN5G62L7F1OiJ5nnA&q=' + searchTerm)
      .then(resp => resp.json())
      .then(json => resolve(json.data[0].embed_url))
      .catch(err => reject(err))
  })
}
http.listen(5000, () => {
  console.log('listening on *:3000')
})
